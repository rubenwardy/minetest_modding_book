---
title: Getting Started
layout: default
root: ../..
idx: 1.1
description: Learn how to make a mod folder, including init.lua, mod.conf and more.
redirect_from:
- /en/chapters/folders.html
- /en/basics/folders.html
---

## Introduction <!-- omit in toc -->

Understanding the basic structure of a mod's folder is an essential skill when
creating mods. In this chapter, you'll learn about how modding in Minetest works
and create your first mod.

- [What are Games and Mods?](#what-are-games-and-mods)
- [Where are mods stored?](#where-are-mods-stored)
- [Creating your first mod](#creating-your-first-mod)
  - [Mod directory](#mod-directory)
  - [mod.conf](#modconf)
  - [init.lua](#initlua)
  - [Summary](#summary)
- [Dependencies](#dependencies)
- [Mod Packs](#mod-packs)


## What are Games and Mods?

The power of Minetest is the ability to easily develop games without the need
to create your own voxel graphics, voxel algorithms, or fancy networking code.

In Minetest, a game is a collection of modules which work together to provide the
content and behaviour of a game.
A module, commonly known as a mod, is a collection of scripts and resources.
It's possible to make a game using only one mod, but this is rarely done because it
reduces the ease by which parts of the game can be adjusted and replaced
independently of others.

It's also possible to distribute mods outside of a game, in which case they
are also *mods* in the more traditional sense - modifications. These mods adjust
or extend the features of a game.

Both the mods contained in a game and third-party mods use the same API.

This book will cover the main parts of the Minetest API,
and is applicable for both game developers and modders.


## Where are mods stored?

<a name="mod-locations"></a>

Each mod has its own directory where its Lua code, textures, models, and
sounds are placed. Minetest checks in several different locations for
mods. These locations are commonly called *mod load paths*.

For a given world/save game, three mod locations are checked.
They are, in order:

1. Game mods. These are the mods that form the game that the world is running.
   Eg: `minetest/games/minetest_game/mods/`, `/usr/share/minetest/games/minetest/`
2. Global mods, the location to which mods are nearly always installed to.
   If in doubt, place them here.
   Eg: `minetest/mods/`
3. World mods, the location to store mods which are specific to a
   particular world.
   Eg: `minetest/worlds/world/worldmods/`

`minetest` is the user-data directory. You can find the location of the
user-data directory by opening up Minetest and clicking
"Open User Data Directory" in the Credits tab.

When loading mods, Minetest will check each of the above locations in order.
If it encounters a mod with a name the same as one found previously, the later
mod will be loaded in place of the earlier mod. This means that you can override
game mods by placing a mod with the same name in the global mod location.


## Creating your first mod

### Mod directory

Go to the global mods directory (About > Open user data directory > mods) and
create a new folder called "mymod". `mymod` is the mod name.

Each mod should have a unique *mod name*, a technical identifier (id) used to
refer to the mod. Mod names can include letters, numbers, and underscores. A
good name should describe what the mod does, and the directory that contains
the components of a mod must have the same name as the mod name. To find out if
a mod name is available, try searching for it on
[content.minetest.net](https://content.minetest.net).

    mymod
    ├── textures
    │   └── mymod_node.png files
    ├── init.lua
    └── mod.conf

Mods only require an init.lua file;
however, mod.conf is recommended and other components may be needed
depending on the mod's functionality.

### mod.conf

Create a mod.conf file with the following content:

```
name = mymod
description = Adds foo, bar, and bo.
depends = default
```

This file is used for mod metadata including the mod's name, description, and other
information.

### init.lua

Create an init.lua file with the following content:

```lua
print("This file will be run at load time!")

core.register_node("mymod:node", {
    description = "This is a node",
    tiles = {"mymod_node.png"},
    groups = {cracky = 1}
})

core.register_craft({
    type = "shapeless",
    output = "mymod:node 3",
    recipe = { "default:dirt", "default:stone" },
})
```

The init.lua file is the entrypoint to a mod, and runs when the mod is loaded.


### Summary


This mod has the name "mymod". It has two text files: init.lua and mod.conf. The
script prints a message and then registers a node and a craft recipe – these
will be explained later on. There's a single dependency, the
[default mod](https://content.minetest.net/metapackages/default/), which is
usually found in Minetest Game. There is also a texture in textures/ for the
node.


## Dependencies

A dependency occurs when a mod requires another mod to be loaded before itself.
One mod may require another mod's code, items, or other resources to be
available for it to use.

There are two types of dependencies: hard and optional dependencies.
Both require the mod to be loaded first. If the mod being depended on isn't
available, a hard dependency will cause the mod to fail to load, while an optional
dependency might lead to fewer features being enabled.

An optional dependency is useful if you want to optionally support another mod;
it can enable extra content if the user wishes to use both the mods at the same
time.

Dependencies are specified in a comma-separated list in mod.conf.

    depends = modone, modtwo
    optional_depends = modthree

## Mod Packs

Mods can be grouped into mod packs, which allow multiple mods to be packaged
and moved together. They are useful if you want to supply multiple mods to
a player, but don't want to make them download each one individually.

    modpack1
    ├── modpack.conf (required) - signals that this is a mod pack
    ├── mod1
    │   └── ... mod files
    └── mymod (optional)
        └── ... mod files

Please note that a modpack is not a *game*.
Games have their own organisational structure which will be explained in the
Games chapter.
